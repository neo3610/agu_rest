from django.contrib import admin
from django.urls import path, include
from some_app.routing import urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(urlpatterns))
]
