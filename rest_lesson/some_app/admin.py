from django.contrib import admin

# Register your models here.
from some_app.models import Comment, Article

admin.site.register(Article)
admin.site.register(Comment)
